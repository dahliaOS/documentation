<!-- This is the TOC that appears on the website. 
Links should use /docs and not .md extension. 
V 7.2.9-->
## Table of Contents

- [Supported devices](docs/supported-devices.md)
- [Hardware](docs/hardware)
  * [Example file](docs/hardware/example%20file.md)
- [Run dahliaOS](docs/run%20dahliaOS)
  * [x86_64-efi](docs/run%20dahliaOS/x86_64-efi.md)
  * [x86_32-efi](docs/run%20dahliaOS/x86_32-efi.md)
  * [x86_64-legacy](docs/run%20dahliaOS/x86_64-legacy.md)
  * [QEMU](docs/run%20dahliaOS/qemu.md)
  * [AEMU](docs/run%20dahliaOS/aemu.md)
  * [PEMU](docs/run%20dahliaOS/pemu.md)
- [Pangolin](docs/pangolin)
  * [Build Pangolin](docs/pangolin/build_pangolin.md)
  * [Pangolin Desktop](docs/pangolin/Pangolin-Desktop.md)
  * [Pangolin Linux](docs/pangolin/Pangolin-Linux.md)
- [OS](docs/os)
  * [Build](docs/os/build) 
    * [Building an ISO for your device](docs/os/build/building-an-iso-for-your-device.md)
    * [Buildroot](docs/os/build/buildroot.md)
  * [Catalog](docs/os/catalog)
    * [Catalog](docs/os/catalog/catalog.md)
  * [Linux-based system](docs/os/linux-based.md)
  * [Translations](docs/os/translations)
    * [Translation](docs/os/translations/translation.md)
- [Contributing](docs/CONTRIBUTING.md)
