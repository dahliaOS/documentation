# Catalog

## Official

|Version|Download|Size|Release Date|      
| -----------  | -----------  | ----------- | ----------- |  
|[201215](https://github.com/dahliaos/releases/releases/tag/201215-x86_64)|[`efi`](https://github.com/dahliaos/releases/releases/download/201215-x86_64/dahliaOS-201215-efi.zip)  [`legacy`](https://github.com/dahliaos/releases/releases/download/201215-x86_64/dahliaOS-201215-legacy.iso)|282 MB|15 Dec `2020`
|[201004](https://github.com/dahliaos/releases/releases/tag/201004-x86_64)|[`efi`](https://github.com/dahliaos/releases/releases/download/201004-x86_64/dahliaOS-201004-efi.zip)  [`legacy`](https://github.com/dahliaos/releases/releases/download/201004-x86_64/dahliaOS-201004-legacy.iso)|158 MB|4 Oct `2020`  
|[200830](https://github.com/dahliaos/releases/releases/tag/200830-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200830-x86_64/dahliaOS-200830.iso)|85 MB|30 Aug `2020`  
|[200804](https://github.com/dahliaos/releases/releases/tag/200804-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200804-x86_64/dahliaOS-200804.iso)|104 MB|5 Aug `2020`  
|[200630_2](https://github.com/dahliaos/releases/releases/tag/200630.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200630.1-x86_64/dahliaOS-200630_2.iso)|163 MB|30 Jun `2020`  
|[200614_1](https://github.com/dahliaos/releases/releases/tag/200614.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200614.1-x86_64/dahliaOS-200614r1.iso)|174 MB|15 Jun `2020`  
|[200507_1](https://github.com/dahliaos/releases/releases/tag/200507.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200507.1-x86_64/dahliaOS200507-1.iso)|659 MB|8 May `2020`  
|[200506_1](https://github.com/dahliaos/releases/releases/tag/200506.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200506.1-x86_64/dahliaOS200506-1.iso)|622 MB|7 May `2020`  
|[200407_1](https://github.com/dahliaos/releases/releases/tag/200407.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200407.1-x86_64/dahliaOS200407-1.iso)|251 MB|8 Apr `2020`  
|[200403_1](https://github.com/dahliaos/releases/releases/tag/200403.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200403.1-x86_64/dahliaOS200403-1.iso)|289 MB|3 Apr `2020`  
|[200402_1](https://github.com/dahliaos/releases/releases/tag/200402.1-x86_64)|[`download`](https://github.com/dahliaos/releases/releases/download/200402.1-x86_64/dahliaOS200402-1.iso)|290 MB|3 Apr `2020`  

## 3rd party Grub builds

|Version|Download|Size| Release Date|      
| -----------  | -----------  | ----------- | ----------- |  
|[201215](https://github.com/HexaOneOfficial/dahliaos/releases/tag/201215)|[`Download`](https://github.com/HexaOneOfficial/dahliaos/releases/download/201215/DahliaOS201215.iso)|193 MB |1 Jan `2021` 
|[201004](https://github.com/HexaOneOfficial/dahliaos/releases/tag/201004)|[`Download`](https://github.com/HexaOneOfficial/dahliaos/releases/download/201004/DahliaOS201004.iso)|160 MB |9 Nov `2020`  
|[200830](https://github.com/HexaOneOfficial/dahliaos/releases/tag/200830)|[`Download`](https://github.com/HexaOneOfficial/dahliaos/releases/download/200830/DahliaOS200830.iso)|112 MB |16 Sep `2020`  
|[200804](https://github.com/HexaOneOfficial/dahliaos/releases/tag/200804)|[`Download`](https://github.com/HexaOneOfficial/dahliaos/releases/download/200804/DahliaOS200804.iso)|150 MB |5 Aug `2020`  
|[200630_2](https://github.com/HexaOneOfficial/dahliaos/releases/tag/200630_2)|[`Download`](https://github.com/HexaOneOfficial/dahliaos/releases/download/200630_2/DahliaOS200630_2.iso)|192 MB |10 Jul `2020`

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](../../LICENSE)
