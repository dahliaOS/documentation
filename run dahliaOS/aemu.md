# AEMU

- AEMU is a graphical Fuchsia emulator.

- It is capable of running the vullkan driver but you must install vullkan first. 

- AEMU is capable of running flutter GUI applications such as Ermine and [pangolin-desktop](https://github.com/dahliaos/pangolin-desktop).

## Linux

`fx aemu`

## macOS

`fx aemu -x`

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](../LICENSE)
