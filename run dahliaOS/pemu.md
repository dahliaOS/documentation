# Prickle emulator

Prickle emulator is dahliaOS' virtual machine (similar to Machina which is like terminal for Fuchsia) which is very unfishished at this stage, you can find it [here](https://github.com/dahliaos/prickle-emulator).

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](../LICENSE)
