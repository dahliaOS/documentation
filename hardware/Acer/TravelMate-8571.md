# Acer TravelMate 8571 documentation

Note: This is not an officially supported device for the zircon version, only for the Linux version.

- The legacy builds from 201004 onwards can run on it by default.

## Known problems
- The fingerprint driver doesn't work out of the box yet.
- The webcam may not be optimized out of the box.

## Install
- You can follow the install instructions [here](../../run%20dahliaOS/x86_64-legacy.md). 

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](../../LICENSE)
