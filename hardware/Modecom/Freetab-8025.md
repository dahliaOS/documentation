# Modecom Freetab 8025

Note: This is not an officially supported device for the zircon version, only for the Linux version.

- Requires a third party build.

## Known problems
- Touchscreen doesn't work.
- Camera doesn't work (May work with Linux 5.8+).
- Support for UEFI-32 boot needs to be added in order to work. 

## Install
- You can follow the install instructions [here](../../run%20dahliaOS/x86_32-efi.md). 

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](../../LICENSE)
