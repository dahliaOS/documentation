<!-- 
- Please make sure you read and fill this template out entirely
- PRs that don't have the template filled out will be ignored 
-->

## Description

- Summary of the change (What have you documented? What have you corrected?):
- Why is this change required?: 

## Type of change

<!-- Please tick the relevant option by putting an X inside the bracket without whitespaces -->

- [ ] Typo/grammar correction
- [ ] Documented an existing feature
- [ ] Documented a new feature
- [ ] Documented a new supported device

## Checklist:

- [ ] I have performed a self-review of my own text.
- [ ] I have checked my text and corrected any misspellings.
- [ ] I have read the [CONTRIBUTING](../CONTRIBUTING.md) document.
