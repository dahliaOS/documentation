<p align="center">
  <img width="80%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
<p>
<div align="center">
  <a href="https://dahliaos.io">Website</a> ●
  <a href="https://discord.gg/7qVbJHR">Discord</a> ●
  <a href="https://github.com/dahliaos/releases/releases">Releases</a> ●
  <a href="https://opencollective.com/dahliaos">Donate</a> ●
  <a href="https://github.com/dahliaos/pangolin-desktop">Pangolin</a>
</div>

# Documentation
![Release-stable](https://img.shields.io/github/v/release/HexaOneOfficial/documentation.svg) ![License](https://img.shields.io/github/license/dahliaos/documentation?color=bright-green) 

- **Documentation** for the dahliaOS project

## Table of Contents

- [Supported devices](../supported-devices.md)
- [Hardware](../hardware)
  * [Example file](../hardware/example%20file.md)
- [Run dahliaOS](../run%20dahliaOS)
  * [x86_64-efi](../run%20dahliaOS/x86_64-efi.md)
  * [x86_32-efi](../run%20dahliaOS/x86_32-efi.md)
  * [x86_64-legacy](../run%20dahliaOS/x86_64-legacy.md)
  * [QEMU](../run%20dahliaOS/qemu.md)
  * [AEMU](../run%20dahliaOS/aemu.md)
  * [PEMU](../run%20dahliaOS/pemu.md)
- [Pangolin](../pangolin)
  * [Build Pangolin](../pangolin/build_pangolin.md)
  * [Pangolin Desktop](../pangolin/Pangolin-Desktop.md)
  * [Pangolin Linux](../pangolin/Pangolin-Linux.md)
- [OS](os)
  * [Build](../os/build) 
    * [Building an ISO for your device](../os/build/building-an-iso-for-your-device.md)
    * [Buildroot](../os/build/buildroot.md)
  * [Catalog](../os/catalog)
    * [Catalog](../os/catalog/catalog.md)
  * [Linux-based system](../os/linux-based.md)
  * [Translations](../os/translations)
    * [Translation](../os/translations/translation.md)
- [Contributing](../CONTRIBUTING.md)

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](../LICENSE)
