# Supported Hardware

`Legacy`: can only run Legacy builds, `EFI`: can only run EFI builds, Those with no tag can run both.

| Brand| Device|Compatible Build|Documentation|    
| -----------  | -----------  | ----------- | ----------- | 
|Acer|Switch Alpha 12|Zircon|[`Link`](https://fuchsia.dev/docs/development/hardware/acer12.md)
|Acer|Aspire timelineX 5830-series|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|[`Link`](hardware/Acer/Aspire-timelineX-5830-series.md)
|Acer|TravelMate 8571|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|[`Link`](hardware/Acer/TravelMate-8571.md)
|Acer|TravelMate P455-M|Linux|[`Link`](hardware/Acer/TravelMate-P455-M.md)
|Acer|Travelmate p645-S|Linux|[`Link`](hardware/Acer/TravelMate-P645-S.md)|
|Acer|Chromebook 14 CB3-431|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|N/A
|Asus|M2N68-AM Plus, Athlon II X3|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|N/A
|Apple|M1|Linux|[`build`](https://github.com/dahliaOS/buildroot/releases/tag/M1-210128)
|Apple|Pre 2017 Macbook Air|Linux|N/A
|Apple|Mid 2012 15" Macbook Pro|Linux|N/A
|Apple|Late 2009 13" Macbook|Linux [`EFI`](run%20dahliaOS/x86_64-efi.md)|N/A
|Apple|Early 2014 Macbook Air|Linux|[`Link`](hardware/Apple/Macbook-air-early-2014.md)
|Broadcom|Raspberry Pi 4|Linux, Zircon|N/A
|Broadcom|Raspberry Pi 3|Linux|N/A
|Dell|Optiplex 790|Linux, Zircon|N/A
|Dell|Chromebook 11 3180|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|N/A
|Dell|Chromebook 11 3120 P22T|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|N/A
|Gigabyte|GA-EP45-UD3P, Core 2 Quad, GT710|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|N/A
|Google|Pixelbook|Zircon|[`Link`](https://fuchsia.dev/docs/development/hardware/pixelbook.md)
|HP|Compaq 8200-Elite|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|[`Link`](hardware/HP/Compaq-8200-Elite.md)
|HP|Compaq Pro 6300|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|[`Link`](hardware/HP/Compaq-Pro-6300.md)
|HP|Elitedesk 800-G1-SFF|Linux|[`Link`](hardware/HP/Elitedesk-800-G1-SFF.md)
|HP|630|Linux [`Legacy`](run%20dahliaOS/x86_64-legacy.md)|[`Link`](hardware/HP/630.md)
|HP|ProDesk 490-G1-MT|Linux|[`Link`](hardware/HP/ProDesk-490-G1-MT.md)
|HP|Pavilion Laptop 15-cs0xxx|Linux [`EFI`](run%20dahliaOS/x86_64-efi.md), Zircon|[`Link`](hardware/HP/Pavilion-Laptop-15-cs0xxx.md)
|Intel|HackBoard HB2|Linux, Zircon `untested`|N/A
|Intel|LattePanda V1|Linux, Zircon|N/A
|Intel|Nuc|Zircon|[`Link`](https://fuchsia.dev/docs/development/hardware/developing_on_nuc.md)
|JetWay|HBJC130F731 Series|Zircon|[`Link`](https://fuchsia.dev/fuchsia-src/development/hardware/toulouse)
|Khadas|Vim 2|Zircon|[`Link`](https://fuchsia.dev/docs/development/hardware/khadas-vim)
|Lenovo|Flex 3 80R3|Linux|[`Link`](hardware/Lenovo/Flex-3-80R3.md)
|Lenovo|ThinkCentre M90n IoT|Zircon|N/A
|Modecom|Freetab 8025|Linux|[`Link`](hardware/Modecom/Freetab-8025.md)
|NXP|iMX8M EVK|Zircon|[`Link`](https://fuchsia.dev/fuchsia-src/development/hardware/imx8mevk)
|Pine64|Pinephone|Zircon|[`Link`](hardware/pine64/Pinephone.md)
|Samsung|Series 7 700Z5C|Linux|N/A
|Toshiba|Satellite E45W-C4200X|Linux [`EFI`](run%20dahliaOS/x86_64-efi.md), Zircon|N/A
|96boards|HiKey960|Zircon|[`Link`](https://fuchsia.dev/fuchsia-src/development/hardware/hikey960)

## License

<p align="left">
  <img width="45%" src="https://github.com/dahliaos/brand/blob/master/Logo%20SVGs/dahliaOS%20logo%20with%20text%20(drop%20shadow).svg"
</p>

Copyright © The dahliaOS authors, contact@dahliaos.io

This project is licensed under the [Apache 2.0 license](LICENSE)
